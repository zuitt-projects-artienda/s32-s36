const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

/*
	Notes: 
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us to authorize our users to access or disallow access to certain parts of our app.

	JWT is like a gift wrapping service that is able to encode our user details which can only be unwrapped by jwt own methods and if the secret provided is intact.

	If JWT seemed tampered with, we will reject the users attempt to access a feature.
*/

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//.sign method will create a token
	return jwt.sign(data, secret, {})
};

/*
	Notes:
	>>You can only get a unique jwt with out secret if you log in to our app with the correct email and password.

	>>As a user, you can get your own details from your own token from loggin in.

	>> JWT is not meant to store sensitive data. For now, for ease of use and for our MVP (min variable product), we add the email and isAdmin details of the logged in user. However, in the future, you can limit this to only id and or every route and feature, you can simply look up for the user in the database to get his details.

	>>We will verify the legitimacy of a JWT every time a user access a restricted feaure. Each JWT contains a secret only our server knows. if he JWT has beem changed in any way, we will reject the user and his tampered token. If the JWT does not contain a secret OR the secret is different, we will reject his access and token.
*/

module.exports.verify = (req, res, next) => {

	//req.headers.authorization contains sensitive data and especially our token.jwt

	let token = req.headers.authorization
	console.log(token)

	if(typeof token === "undefined"){
		return res.send({auth: "Failed. No Token"})
	} else {
		//Bearer eyHoihfeoh- it will remove Bearer
		token = token.slice(7, token.length)

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			} else {

				req.user = decodedToken;
				next();
			}

		})
	}
};

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();
	} else {
		return res.send({
			auth: "Failed",
			message: "Forbidden Action"
		})
	}

};