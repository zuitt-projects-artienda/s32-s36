const express = require('express');

const router = express.Router();

const userControllers = require('../controllers/userControllers')

const auth = require("../auth");

const {verify, verifyAdmin} = auth;

//Routes

//User Registration

router.post("/", userControllers.registerUser);

//Get all users
router.get("/", userControllers.getAllUsers);

//Log in route
router.post("/login", userControllers.loginUser);

//Retrieving user detials

router.get("/getUserDetails",verify, userControllers.getUserDetails);

//Check Email Exists

router.post('/checkEmailExists/', userControllers.checkEmailExists);

//CheckID
router.put("/updateAdmin/:id", verify, verifyAdmin, userControllers.updateAdmin);

//update user details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//Enrollment
router.post("/enroll", verify, userControllers.enroll);

// Get Enrollments

router.get("/getEnrollments", verify, userControllers.getEnrollments);





module.exports = router;