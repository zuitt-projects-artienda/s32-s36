const Course = require('../models/Course');

const auth = require('../auth');

module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})
	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};


module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

//Activity 3
module.exports.getSingleCourse =(req, res) => {
	
	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))

};

// Acitivity 4

module.exports.archiveCourse = (req, res) => {

	// console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUsers => res.send(updatedUsers))
	.catch(err => res.send(err));
};

module.exports.activateCourse = (req, res) => {

	// console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUsers => res.send(updatedUsers))
	.catch(err => res.send(err));
};

	
module.exports.getActiveCourses = (req, res) => {

	Course.find({})
	.then(courses => {
		let filtered = courses.filter(course => {
			return course['isActive']
		})
		res.send(filtered)
	})
	.catch(err => res.send(err));
};

// module.exports.getActiveCourses = (req, res) => {

// 	Course.find({isActive: true})
// 	.then(result => res.send(result))
// 	.catch(err => res.send(err))
// };

// Update course

module.exports.updateCourse = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourses => res.send(updatedCourses))
	.catch(err => res.send(err));

};

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

// find courses by name
module.exports.findCoursesByName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options: "$i"}})
	.then(result => {
		if(result.length === 0){
			return res.send("No courses found");
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err));
};	

//find courses by price

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({price: req.body.price})
	.then(result => {

		console.log(result)
		if(result.length === 0){
			return res.send("No course found");
		
		} else {
			return res.send(result);
		}
	})
	.catch(err => res.send(err));
};


module.exports.getEnrollees =(req, res) => {
	
	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => {
		if(result.enrollees.length === 0){
			return res.send("No enrolles found");
		} else {
			res.send(result.enrollees)
		}
	})
	.catch(error => res.send(error))

};
