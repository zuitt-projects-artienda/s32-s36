//import bcrypt module
// bcrypt- storing password for more security
const bcrypt = require('bcrypt');

//Import User Model
const User = require('../models/User');

//Import Course Model
const Course = require("../models/Course");

//import autho module
const auth = require('../auth');

//Controllers

//User registration

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	//hashSync(x,y)- needs two parameter
	//bcrypt- add a layer of security to your user's password
	// bcrypt does is hash our password into a randomized character version of the original string.
	//syntax: bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
	//Sale-Rounds are the number of times the character in the hash are randomized
	const hashedPW = bcrypt.hashSync(req.body.password, 10)

	//create a new user doc

	let newUser = new User({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo
	})
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

//Retrieve all users
module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

//Login

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		1. find user email
		2. if user email is found, check the password
		3. if email doesnt exist, send a message
		4. if upon checking the found users password is the same as our input password we will generate a "key" to access our app. If not, we will turn him away by sending a message to client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if(foundUser === null){
			return res.send("User does not exist");
		} else {
			//bcrypt.compareSync()- to compare req password and database encrypt pass
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})
			} else {
				return res.send("Password is incorrect")
			}
		}
	})
	.catch(err => res.send(err));
};

module.exports.getUserDetails = (req, res) => {

	console.log(req.user)
	
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};


//Activity 3

module.exports.checkEmailExists =(req, res) => {
	
	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundEmail => {
		if(foundEmail === null){
			return res.send("Email is available");
		} else {
			return res.send("Email is already registered")
		}
	})
	.catch(error => res.send(error))

};

// module.exports.checkEmailExists = (req, res) => {
	
// 	console.log(req.body);

// 	User.findOne(req.body.email)
// 	.then(result => res.send(result))
// 	.catch(err => res.send(err));
// };

//Updating regular user to admin

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUsers => res.send(updatedUsers))
	.catch(err => res.send(err));
};

module.exports.updateUserDetails = (req, res) => {

	//checking the input for new values for our user's details.
	console.log(req.body);
	//check the loggedin user's id
	console.log(req.user.id);

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));

};

// Eroll a user

module.exports.enroll = async (req, res) => {
	/*
		Enrollment Process:

		1. Look for the user by its ID.
			>> push the details of the course we're trying to enroll in.
			>> We'll push to a new enrollment subdocument in our user.
		2. Look for the course by its ID.
			>> push the details of the enrolle/user who's trying to enroll.
			>> We'll push to a new enrollees subdocument in our course.
		3. When both saving documents are successful, we send a message to the client.		
	*/
	console.log(req.user.id);
	console.log(req.body.courseId);

	//Checking is a user is an Admin.
	//Admin should not be able to enroll
	if (req.user.isAdmin){
		return res.send("Action Forbidden");
	};
	/*
		Find the user:

		async: a keyword that allows us to make our function asunchronous. Which means, that instead of JS regular behavoir of running each code by line by line, it will allow us to wait for the result of the function.

		await: a keyword that allows us to wait for the function to finish before proceeding

	*/
	let isUserUpdated = await User.findById(req.user.id).then(user => {

		console.log(user);

		//add the couseId in an object and push that object into user's enrollment arrays
		let newEnrollment = {
			courseId: req.body.courseId
		}
		user.enrollments.push(newEnrollment);
		return user.save().then(user => true).catch(err => err.message);
	})
	//if isUserUpdated does not contain the boolean value of true, we will stop our porcess and return res.send() to our client with our message
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	// create an object which contain the userid f the enrollee of a course.
	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

		console.log(course);

		let enrollee = {
			userId: req.user.id
		}
		course.enrollees.push(enrollee);

		return course.save().then(course => true).catch(err => err.message);
	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: 'Enrolled Successfully!'})
	}
};

// Get enrollments



module.exports.getEnrollments = (req, res) => {
	
	User.findById(req.user.id)
	.then(result => {
		if(result.enrollments.length === 0){
			return res.send("Not yet enrolled");
		} else {
			res.send(result.enrollments)
		}
	})
	.catch(err => res.send(err));
};

