//create server
const express = require('express'); 
//allows our backend application to be available in our frontend application.
//cors- cross origin resource sharing
const cors = require('cors'); 

const userRoutes = require('./routes/userRoutes');
const courseRoutes = require('./routes/courseRoutes');

//dataabse
const mongoose = require('mongoose'); 

const app = express();

const port = 4000;

mongoose.connect("mongodb+srv://admin_artienda:admin169@cluster0.mtvrk.mongodb.net/bookingAPI169?retryWrites=true&w=majority",{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));

db.once('open', () => console.log("Connected to MongoDB"));

app.use(express.json());
//added cors 
app.use(cors());
//use our routes and group together under '/users'
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);


app.listen(port, () => console.log(`Server is running on localhost: ${port}`))

